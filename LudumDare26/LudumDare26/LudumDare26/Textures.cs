﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace LudumDare26
{
    class Textures
    {
        public Textures(ContentManager content)
        {
            this.Pixel0 = content.Load<Texture2D>("Sprites\\Pixel0");
            this.Ground0 = content.Load<Texture2D>("Sprites\\Ground0");
            this.Ground1 = content.Load<Texture2D>("Sprites\\Ground1");
            this.CorruptedPixel3 = content.Load<Texture2D>("Sprites\\CorruptedPixel3");
            this.CorruptedPixel0 = content.Load<Texture2D>("Sprites\\CorruptedPixel0");
            this.CorruptedPixel1 = content.Load<Texture2D>("Sprites\\CorruptedPixel1");
            this.SelectionBorder = content.Load<Texture2D>("Sprites\\Border");
            this.Ressources = content.Load<Texture2D>("Sprites\\Ressources");
            this.Projectile = content.Load<Texture2D>("Sprites\\Projectile");
            this.Wall = content.Load<Texture2D>("Sprites\\Wall");
            this.Map0 = content.Load<Texture2D>("Sprites\\Map0");
            this.Button = content.Load<Texture2D>("Sprites\\Button");
            this.Potato = content.Load<Texture2D>("Sprites\\Potato");
            this.NoPotato = content.Load<Texture2D>("Sprites\\NoPotato");
            this.SpawnBorder = content.Load<Texture2D>("Sprites\\BorderSpawn");
            this.Maps = new Texture2D[] {
                content.Load<Texture2D>("Sprites\\Map0"),
                content.Load<Texture2D>("Sprites\\Map1"),
                content.Load<Texture2D>("Sprites\\Map2"),
                content.Load<Texture2D>("Sprites\\Map3"),
                content.Load<Texture2D>("Sprites\\Map4"),
            };
        }
        public Texture2D Pixel0;
        public Texture2D CorruptedPixel0;
        public Texture2D CorruptedPixel1;
        public Texture2D CorruptedPixel3;
        public Texture2D Ground0;
        public Texture2D Ground1;
        public Texture2D SelectionBorder;
        public Texture2D Ressources;
        public Texture2D Projectile;
        public Texture2D Wall;
        public Texture2D Map0;
        public Texture2D Button;
        public Texture2D Potato;
        public Texture2D NoPotato;
        public Texture2D SpawnBorder;
        public Texture2D[] Maps;
    }
}
