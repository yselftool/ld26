using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LudumDare26
{
    public class LD26Game : Microsoft.Xna.Framework.Game
    {
        // General
        GameState gameState = GameState.Menu;
        RoundMode roundMode = RoundMode.Endless;
        Int32 currentMap = 0;
        
        // Graphics
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Point screenSize = new Point(1024, 768);
        SpriteFont font, scoreFont, aboutFont, tutorialFont, titleFont;
        Textures textures;

        // Camera
        Vector2 cameraPosition;
        Rectangle borders;
        Int32 cameraScrollSpeed = 4;

        // Controls
        KeyboardState currentKeyboardState;
        KeyboardState previousKeyboardState;
        MouseState currentMouseState;
        MouseState previousMouseState;
        GamePadState currentGamePadState;
        GamePadState previousGamePadState;

        // Menu
        Button[] menuButtons;
        Button startEndlessGameButton;
        Button startCampaignGameButton;
        Button exitGameButton;
        Button reloadScoresButton;
        Button aboutButton;

        String menuAboutText = "Defense of the corrupted Pixels\n(c) by ToolDev.de, made for LD26";
        String tutorialText = "turn-based Mobile-Tower-Defense game\n\n" +
            "Your screen is broken!\n Corrupted Pixels have appeared!\n" +
            "Use your sane pixels to defend against them!\n" +
            " RED pixels are fast,\n GREEN pixels are powerful,\n BLUE pixels have much life\n" +
            "Pixels can merge and become better\n" +
            "Nobody can move trough broken pixels\n" + 
            "Your pixels may become weak and break\n\n" +
            "left-click: select ; right-click: move\n" +
            "mouse wheel / arrow keys: scroll\n" +
            "[SPACE]: spawn new pixels (bottom end) or end turn\n" + 
            "Hover a pixel for information";

        // Units
        List<Unit> units;

        // Enemies
        List<Unit> enemies;
        List<Unit> enemyWave;
        Int32 waveCount = 1;

        // Projectiles
        List<Projectile> projectiles;

        // Ressources
        Ressources ressources;

        // Ground
        GroundTile[,] ground;
        Point groundSize;
        Int32 tileSize = 32;

        // Selection
        Unit selectedUnit;

        // Random
        Random random = new Random();

        // Rounds end
        Unit currentUpdatedUnit;
        TimeSpan elapsedUpdateTime;
        TimeSpan targetUpdateSpan;

        // Messages
        String message = "";
        Int32 messageTimer = 0;

        // Music
        Song[] songs;
        TimeSpan elapsedMusicTime;
        Song currentSong;

        // Winning Screen
        TimeSpan winningScreenElapsedTime = new TimeSpan();
        Boolean noPotato = true;

        // Sounds
        SoundEffect nextLevelSound, shotSound, spawnSound;
        SoundEffectInstance nextLevelSoundInstance, shotSoundInstance, spawnSoundInstance;
        float soundVolume = 0.5f;


        // Highscore
        HighscoreManager highscoreManager;
        Int32 score = 0;

        public LD26Game()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {

            graphics.PreferredBackBufferWidth = screenSize.X;
            graphics.PreferredBackBufferHeight = screenSize.Y;
            graphics.IsFullScreen = false;
            graphics.ApplyChanges();
            this.IsMouseVisible = true;
            this.Window.Title = ".cp";

            // Controls
            currentKeyboardState = new KeyboardState();
            previousKeyboardState = new KeyboardState();
            currentMouseState = new MouseState();
            previousMouseState = new MouseState();
            currentGamePadState = new GamePadState();
            previousGamePadState = new GamePadState();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            // Font
            font = Content.Load<SpriteFont>("Font\\Font");
            scoreFont = Content.Load<SpriteFont>("Font\\Score");
            aboutFont = Content.Load<SpriteFont>("Font\\About");
            tutorialFont = Content.Load<SpriteFont>("Font\\Tutorial");
            titleFont = Content.Load<SpriteFont>("Font\\Title");

            // Camera
            cameraPosition = new Vector2(0, 0);
            
            // Textures
            textures = new Textures(Content);
            
            // Menu
            startEndlessGameButton = new Button(textures.Button, new Point(0, 100 * 0 + 160), "Start Game", new Color(0, 255, 0));
            startCampaignGameButton = new Button(textures.Button, new Point(0, 100 * 1 + 160), "Start Campaign", new Color(0, 0, 255));
            aboutButton = new Button(textures.Button, new Point(0, 100 * 2 + 160), "About", new Color(0, 255, 255));
            exitGameButton = new Button(textures.Button, new Point((Int32)(0.00f * screenSize.X), 100 * 3 + 160), "Exit", new Color(255, 0, 0));
            
            reloadScoresButton = new Button(textures.Button, new Point((Int32)(0.77f * screenSize.X - 10 ), 436), "Reload Scores", new Color(255, 255, 0));
            
            menuButtons = new Button[]
            {
                startEndlessGameButton,
                startCampaignGameButton,
                aboutButton,
                exitGameButton,
                reloadScoresButton,
            };

            // Ressources
            this.ressources = new Ressources(10, 10, 10);

            // Units
            this.units = new List<Unit>();
            
            // Enemies
            enemies = new List<Unit>();
            enemyWave = new List<Unit>();

            // Projectiles
            projectiles = new List<Projectile>();

            // Selection
            selectedUnit = null;

            // Update
            elapsedUpdateTime = new TimeSpan();
            targetUpdateSpan = TimeSpan.FromMilliseconds(500);

            // Music
            MediaPlayer.Volume = 0.5f;
            songs = new Song[] {
                Content.Load<Song>("Music\\Disco#1"),
                Content.Load<Song>("Music\\March#1"),
                Content.Load<Song>("Music\\Piano#1"),
                Content.Load<Song>("Music\\Rock#1"),
                Content.Load<Song>("Music\\Piano#2")
            };
#if !DEBUG
            PlaySong(songs[random.Next(songs.Length)]);
#endif

            // Sound
            nextLevelSound = Content.Load<SoundEffect>("Sounds\\NextLevel");
            nextLevelSoundInstance = nextLevelSound.CreateInstance(); 
            shotSound = Content.Load<SoundEffect>("Sounds\\Shot");
            shotSoundInstance = shotSound.CreateInstance();
            spawnSound = Content.Load<SoundEffect>("Sounds\\Spawn");
            spawnSoundInstance = spawnSound.CreateInstance();
            UpdateSoundVolume();

            // Highscore
            highscoreManager = new HighscoreManager();
            highscoreManager.BeginPull();
        }

        private void UpdateSoundVolume()
        {
            nextLevelSoundInstance.Volume = soundVolume;
            spawnSoundInstance.Volume = soundVolume;
            shotSoundInstance.Volume = soundVolume;
        }

        private void CreateGround(Texture2D map)
        {
            groundSize = new Point(map.Width, map.Height);
            Color[] colors = new Color[groundSize.X * groundSize.Y];
            Color wallColor = new Color(0,0,0);
            Color enemyColor = new Color(255, 0, 0);
            map.GetData<Color>(colors);
            ground = new GroundTile[groundSize.X, groundSize.Y];
            for (Int32 x = 0; x < groundSize.X; x++)
                for (Int32 y = 0; y < groundSize.Y; y++)
                {
                    GroundTile tile = tile = new GroundTile(textures.Ground1, false, new Color(random.Next(32) + 32, random.Next(32) + 32, random.Next(32) + 32)); ;
                    Color c = colors[y * groundSize.X + x];
                    if (c == wallColor)
                    {
                        tile = new GroundTile(textures.Wall, true, new Color(255, 255, 255));
                    }
                    else if (c == enemyColor)
                    {
                        enemies.Add(Unit.CreateRandom(random, textures.CorruptedPixel3, new Point(x, y)));
                    }
                    ground[x, y] = tile;
                }
            borders = new Rectangle(0, 0, groundSize.X * tileSize, groundSize.Y * tileSize);
        }

        private void CreateGround(Point size)
        {
            groundSize = size;
            ground = new GroundTile[groundSize.X, groundSize.Y];
            for (Int32 x = 0; x < groundSize.X; x++)
                for (Int32 y = 0; y < groundSize.Y; y++)
                {
                    ground[x, y] = new GroundTile(textures.Ground1, false, new Color(random.Next(32) + 32, random.Next(32) + 32, random.Next(32) + 32));
                }
            Int32 wallCount = random.Next(8) + 4;
            for (Int32 i = 0; i < wallCount; i++)
            {
                Int32 y = (groundSize.Y / wallCount) * i + 4;
                y += random.Next(2);
                if (y < groundSize.Y - 1)
                {
                    Int32 end = random.Next(2, groundSize.X - 2);
                    if (random.Next(2) == 0)
                    {
                        for (Int32 x = 0; x < end; x++)
                            ground[x, y] = new GroundTile(textures.Wall, true, new Color(255, 255, 255));
                    }
                    else
                    {
                        for (Int32 x = groundSize.X - 1; x > end; x--)
                            ground[x, y] = new GroundTile(textures.Wall, true, new Color(255, 255, 255));
                    }
                }
            }
            borders = new Rectangle(0, 0, groundSize.X * tileSize, groundSize.Y * tileSize);
        }

        protected override void UnloadContent()
        {
            if (!highscoreManager.PushedOne && score > 0)
                highscoreManager.BeginPush(Tuple.Create(Environment.UserName, score));

            if (!nextLevelSoundInstance.IsDisposed)
            {
                nextLevelSoundInstance.Stop();
                nextLevelSoundInstance.Dispose();
            }
            nextLevelSound.Dispose();
            if (!shotSoundInstance.IsDisposed)
            {
                shotSoundInstance.Stop();
                shotSoundInstance.Dispose();
            }
            shotSound.Dispose();
            if (!spawnSoundInstance.IsDisposed)
            {
                spawnSoundInstance.Stop();
                spawnSoundInstance.Dispose();
            }
            spawnSound.Dispose();
            MediaPlayer.Stop();
        }

        protected override void Update(GameTime gameTime)
        {
#if DEBUG
            if (gameTime.TotalGameTime > new TimeSpan(0, 0, 30)) this.Exit();
#endif
            UpdateControls(gameTime);
            UpdateExit(gameTime);
            UpdateMusic(gameTime);
            if(gameState == GameState.Player || gameState == GameState.RoundsEnd || gameState == GameState.Enemy)
                UpdateCamera(gameTime);

            switch(gameState)
            {
                case GameState.Menu:
                    UpdateMenuButtons(gameTime);
                    break;
                case GameState.Player:
                    UpdateSelection(gameTime);
                    UpdateSpawnUnit(gameTime);
                    elapsedUpdateTime += gameTime.ElapsedGameTime;
                    if (elapsedUpdateTime > targetUpdateSpan)
                    {
                        elapsedUpdateTime -= targetUpdateSpan;
                        UpdateMoving(gameTime);
                    }
                    break;
                case GameState.RoundsEnd:
                    elapsedUpdateTime += gameTime.ElapsedGameTime;
                    if (elapsedUpdateTime > targetUpdateSpan)
                    {
                        elapsedUpdateTime -= targetUpdateSpan;
                        UpdateRoundsEnd(gameTime);
                    }
                    break;
                case GameState.Enemy:
                    UpdateProjectiles(gameTime);
                    elapsedUpdateTime += gameTime.ElapsedGameTime;
                    if (elapsedUpdateTime > targetUpdateSpan)
                    {
                        elapsedUpdateTime -= targetUpdateSpan;
                        UpdateEnemy(gameTime);
                    }
                    break;
                case GameState.GameOver:
                    UpdateGameOver(gameTime);
                    break;
                case GameState.WinningScreen:
                    UpdateWinningScreen(gameTime);
                    break;
                case GameState.About:
                    UpdateAbout(gameTime);
                    break;
            }

            UpdateMessage(gameTime);

            base.Update(gameTime);
        }

        private void UpdateWinningScreen(GameTime gameTime)
        {
            winningScreenElapsedTime += gameTime.ElapsedGameTime;
            if (previousKeyboardState.IsKeyDown(Keys.Enter) && currentKeyboardState.IsKeyUp(Keys.Enter))
            {
                gameState = GameState.Menu;
                score = 0;
            }
        }

        private void UpdateAbout(GameTime gameTime)
        {
            if (previousKeyboardState.IsKeyDown(Keys.Enter) && currentKeyboardState.IsKeyUp(Keys.Enter))
            {
                gameState = GameState.Menu;
            }
        }

        private void UpdateMenuButtons(GameTime gameTime)
        {
            if (previousMouseState.LeftButton == ButtonState.Pressed && currentMouseState.LeftButton == ButtonState.Released)
            {
                Point mousePosition = new Point(currentMouseState.X, currentMouseState.Y);
                if (startEndlessGameButton.Rectangle.Contains(mousePosition))
                {
                    roundMode = RoundMode.Endless;
                    StartGame();
                }
                if(exitGameButton.Rectangle.Contains(mousePosition))
                    this.Exit();
                if(reloadScoresButton.Rectangle.Contains(mousePosition))
                    highscoreManager.BeginPull();
                if (aboutButton.Rectangle.Contains(mousePosition))
                    gameState = GameState.About;
                if (startCampaignGameButton.Rectangle.Contains(mousePosition))
                {
                    roundMode = RoundMode.Campaign;
                    currentMap = 0;
                    StartGame();
                }
            }
        }

        private void StartGame()
        {
            if (roundMode == RoundMode.Endless)
                CreateGround(new Point(32, 48));
            else if (roundMode == RoundMode.Campaign)
                CreateGround(textures.Maps[currentMap]);

            gameState = GameState.Player;
            cameraPosition = new Vector2(0, groundSize.Y * tileSize - screenSize.Y);
        }

        private void UpdateGameOver(GameTime gameTime)
        {
            if (previousKeyboardState.IsKeyDown(Keys.Enter) && currentKeyboardState.IsKeyUp(Keys.Enter))
            {
                gameState = GameState.Menu;
            }
        }

        private void PlaySong(Song song)
        {
            elapsedMusicTime = new TimeSpan();
            currentSong = song;
            MediaPlayer.Play(song);
        }

        private void UpdateMusic(GameTime gameTime)
        {
#if !DEBUG
            elapsedMusicTime += gameTime.ElapsedGameTime;
            if (elapsedMusicTime > currentSong.Duration)
            {
                PlaySong(songs[random.Next(songs.Length)]);
            }
#endif
        }

        private void UpdateEnemy(GameTime gameTime)
        {
            if (enemies.Count == 0)
            {
                nextLevelSoundInstance.Play();
                SurvivedRound();
                StartWave();
            }
            else
            {
                Boolean doingAnything = false;
                List<Unit> unitsToRemove = new List<Unit>();
                foreach (Unit unit in enemies)
                {
                    if (unit.TargetPosition == unit.Position)
                    {
                        unit.TargetPosition = new Point(unit.Position.X, groundSize.Y - 1);
                    }
                    if (unit.Position.Y < groundSize.Y - 1)
                    {
                        if (ground[unit.Position.X, unit.Position.Y + 1].IsWall)
                        {
                            if (ground[0, unit.Position.Y + 1].IsWall)
                            {
                                Int32 x;
                                for (x = 0; x < groundSize.X && ground[x, unit.Position.Y + 1].IsWall; x++) { }
                                unit.TargetPosition = new Point(x, unit.Position.Y);
                            }
                            else
                            {
                                Int32 x;
                                for (x = groundSize.X - 1; x >= 0 && ground[x, unit.Position.Y + 1].IsWall; x--) { }
                                unit.TargetPosition = new Point(x, unit.Position.Y);
                            }
                        }
                    }
                    if (unit.WantsUpdate)
                    {
                        doingAnything = true;
                        unit.Update();
                        foreach (Unit u in (from Unit u in enemies where (u != unit && u.Position == unit.Position) select u).ToList())
                        {
                            unit.Combine(u);
                            unitsToRemove.Add(u);
                        }
                        foreach (Unit u in (from Unit u in units where (u.Position == unit.Position) select u).ToList())
                        {
                            unitsToRemove.Add(u);
                            unitsToRemove.Add(unit);
                        }
                        if (unit.Position.Y == groundSize.Y - 1)
                        {
                            GameOver();
                        }
                    }
                    if (unit.Life <= 0)
                    {
                        unitsToRemove.Add(unit);
                        ressources.Red += unit.Color.R / 50;
                        ressources.Green += unit.Color.G / 50;
                        ressources.Blue += unit.Color.B / 50;
                        score += unit.Range > 0 ? unit.Range : 1;
                    }
                }
                foreach (Unit unit in unitsToRemove)
                {
                    if (enemies.Contains(unit))
                    {
                        enemies.Remove(unit);
                    }
                    else if (units.Contains(unit))
                    {
                        units.Remove(unit);
                    }
                }
                foreach(Unit unit in units)
                    foreach (Unit enemy in enemies)
                    {
                        Shoot(unit, enemy);
                    }
                if (!doingAnything)
                {
                    NextState();
                }
            }
        }

        private void GameOver()
        {
            gameState = GameState.GameOver;
            highscoreManager.BeginPush(Tuple.Create(Environment.UserName, score));
            
        }

        private void SurvivedRound()
        {
            if (roundMode == RoundMode.Endless)
            {
                StartMessage("Survive for another wave... gl");
                EnlargeWave();
            }
            else if (roundMode == RoundMode.Campaign)
            {
                currentMap++;
                if (currentMap < textures.Maps.Length)
                {
                    CreateGround(textures.Maps[currentMap]);
                    StartMessage("You saved your screen!\nBut... at the other one, corrupted pixels have appeared.");
                }
                else
                {
                    gameState = GameState.WinningScreen;
                    noPotato = random.Next(2) == 0;
                    highscoreManager.BeginPush(Tuple.Create(Environment.UserName, score));
                }
            }
        }

        private void Shoot(Unit unit, Unit enemy)
        {
            if (unit.Distance(enemy) < unit.Range && unit.Power > 0)
            {

                shotSoundInstance.Play();
                SpawnProjectile(unit, enemy);
                enemy.ReduceHealth(unit.Resistance);
                unit.Power--;
            }
        }

        private void SpawnProjectile(Unit unit, Unit enemy)
        {
            projectiles.Add(new Projectile(new Color((unit.Color.R + enemy.Color.R) / 2, (unit.Color.G + enemy.Color.G) / 2, (unit.Color.B + enemy.Color.B) / 2), 
                (Int32)(targetUpdateSpan.TotalMilliseconds * 0.9f),
                new Vector2(unit.Position.X * tileSize + tileSize / 2, unit.Position.Y * tileSize + tileSize / 2),
                new Vector2(enemy.Position.X * tileSize + tileSize / 2, enemy.Position.Y * tileSize + tileSize / 2)));
        }

        private void UpdateProjectiles(GameTime gameTime)
        {
            List<Projectile> proToRemove = new List<Projectile>();
            foreach (Projectile projectile in projectiles)
            {
                projectile.Timer += (Int32)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (!projectile.Living)
                    proToRemove.Add(projectile);
            }
            foreach (Projectile p in proToRemove)
            {
                projectiles.Remove(p);
            }
        }

        private void StartMessage(String message)
        {
            this.message = message;
            this.messageTimer = 255;
        }

        private void NextState()
        {
            if (gameState == GameState.Player)
            {
                gameState = GameState.RoundsEnd;
                if (units.Count > 0)
                {
                    currentUpdatedUnit = units.First();
                    StartMessage("TURN must END");
                }
                else
                    NextState();
            }
            else if (gameState == GameState.RoundsEnd)
            {
                gameState = GameState.Enemy;
                if ((from Unit unit in enemies
                     where (new Rectangle((Int32)cameraPosition.X, (Int32)cameraPosition.Y, screenSize.X, screenSize.Y)).Contains(new Point(unit.Position.X * tileSize, unit.Position.Y * tileSize))
                     select unit).ToArray().Length == 0)
                {
                    cameraPosition = new Vector2(0, 0);
                }
                StartMessage("ENEMY's TURN");
            }
            else if (gameState == GameState.Enemy)
            {
                gameState = GameState.Player;
                if ((from Unit unit in units
                     where (new Rectangle((Int32)cameraPosition.X, (Int32)cameraPosition.Y, screenSize.X, screenSize.Y)).Contains(new Point(unit.Position.X * tileSize, unit.Position.Y * tileSize))
                     select unit).ToArray().Length == 0)
                {
                    cameraPosition = new Vector2(0, groundSize.Y * tileSize - screenSize.Y);
                }
                StartMessage("YOUR TURN");
                projectiles.Clear();
                foreach (Unit unit in enemies)
                    unit.Reset();
            }
        }

        private void EnlargeWave()
        {
            targetUpdateSpan = TimeSpan.FromMilliseconds(targetUpdateSpan.TotalMilliseconds / (units.Count > 1 ? Math.Log(units.Count) : 1.0d));
            if (enemyWave.Count == 0)
            {
                Int32 m = random.Next(2) + 3;
                for (Int32 i = 0; i < m; i++)
                {
                    enemyWave.Add(Unit.CreateRandom(random, textures.CorruptedPixel3, new Point(random.Next(groundSize.X), 0)));
                }
            }
            else
            {
                foreach (Unit unit in enemyWave)
                {
                    unit.Position = new Point(random.Next(groundSize.X), 0);
                    unit.Life = (Int32)(unit.Life * (1.0f + random.NextDouble() * 2));
                    unit.Power = (Int32)(unit.Power * (1.0f + random.NextDouble() / 2));
                    unit.Resistance = (Int32)(unit.Resistance * (1.0f + random.NextDouble() / 2));
                }
                Int32 m = random.Next(waveCount) + waveCount;
                for (Int32 i = 0; i < m; i++)
                {
                    Unit unit = Unit.CreateRandom(random, textures.CorruptedPixel3, new Point(random.Next(groundSize.X), 0));
                    unit.Speed *= (Int32)(Math.Sqrt(waveCount / 3)) + 1;
                    enemies.Add(unit);
                }
                List<Unit> unitsToRemove = new List<Unit>();
                foreach (Unit unit in units)
                {
                    unit.ReduceAfterRound();
                    if (unit.Life == 0)
                        unitsToRemove.Add(unit);
                }
                foreach (Unit unit in unitsToRemove)
                    units.Remove(unit);
            }
        }

        private void StartWave()
        {
            foreach (Unit unit in enemyWave)
            {
                enemies.Add(unit.Clone());
            }
            waveCount++;
        }

        private void UpdateControls(GameTime gameTime)
        {
            previousKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();
            previousMouseState = currentMouseState;
            currentMouseState = Mouse.GetState();
            previousGamePadState = currentGamePadState;
            currentGamePadState = GamePad.GetState(PlayerIndex.One);
        }

        private void UpdateExit(GameTime gameTime)
        {
            if ((previousKeyboardState.IsKeyDown(Keys.Escape) && currentKeyboardState.IsKeyUp(Keys.Escape)) ||
                (previousGamePadState.Buttons.Back == ButtonState.Pressed && currentGamePadState.Buttons.Back == ButtonState.Released))
                this.Exit();
        }

        private void UpdateCamera(GameTime gameTime)
        {
            if (currentKeyboardState.IsKeyDown(Keys.Right))
                if (cameraPosition.X + screenSize.X + cameraScrollSpeed <= borders.Right)
                    cameraPosition.X += cameraScrollSpeed;
            if (currentKeyboardState.IsKeyDown(Keys.Left))
                if (cameraPosition.X - cameraScrollSpeed >= borders.Left)
                    cameraPosition.X -= cameraScrollSpeed;
            if (currentKeyboardState.IsKeyDown(Keys.Down))
                if (cameraPosition.Y + screenSize.Y + cameraScrollSpeed <= borders.Bottom)
                    cameraPosition.Y += cameraScrollSpeed;
            if (currentKeyboardState.IsKeyDown(Keys.Up))
                if (cameraPosition.Y - cameraScrollSpeed >= borders.Top)
                    cameraPosition.Y -= cameraScrollSpeed;

            Int32 vScrollSpeed = (currentMouseState.ScrollWheelValue - previousMouseState.ScrollWheelValue) / -2;
            if (cameraPosition.Y + screenSize.Y + vScrollSpeed <= borders.Bottom && cameraPosition.Y + vScrollSpeed >= borders.Top)
                cameraPosition.Y += vScrollSpeed;
            if (cameraPosition.Y + screenSize.Y + vScrollSpeed > borders.Bottom)
                cameraPosition.Y = borders.Bottom - screenSize.Y;
            if (cameraPosition.Y + vScrollSpeed < borders.Top)
                cameraPosition.Y = borders.Top;
        }

        private Unit FindUnit(Vector2 position)
        {
            foreach (Unit unit in units)
            {
                Vector2 pos = GetScreenPosition(unit.Position);
                if (pos.X <= currentMouseState.X &&
                    pos.X + unit.Texture.Width >= currentMouseState.X &&
                    pos.Y <= currentMouseState.Y &&
                    pos.Y + unit.Texture.Height >= currentMouseState.Y)
                {
                    return unit;
                }
            }
            return null;
        }

        private void UpdateSelection(GameTime gameTime)
        {
            if (!units.Contains(selectedUnit)) selectedUnit = null;
            if (previousMouseState.LeftButton == ButtonState.Pressed && currentMouseState.LeftButton == ButtonState.Released)
            {
                selectedUnit = FindUnit(new Vector2(currentMouseState.X, currentMouseState.Y));
            }
            if (previousMouseState.RightButton == ButtonState.Pressed && currentMouseState.RightButton == ButtonState.Released)
            {
                if (selectedUnit != null)
                    selectedUnit.TargetPosition = GetTilePosition(new Vector2(currentMouseState.X, currentMouseState.Y));
            }
        }

        private void UpdateSpawnUnit(GameTime gameTime)
        {
            if (previousKeyboardState.IsKeyDown(Keys.Space) && currentKeyboardState.IsKeyUp(Keys.Space))
            {
                Point position = GetTilePosition(new Vector2(currentMouseState.X, currentMouseState.Y));
                if (position.X < 0) position.X = 0;
                if (position.X > groundSize.X - 1) position.X = groundSize.X - 1;
                position.Y = groundSize.Y - 1;
                Unit unit = Unit.Create(ressources, textures.Pixel0, position);
                if (unit != null)
                {
                    spawnSoundInstance.Play();
                    units.Add(unit);
                    foreach (Unit u in (from Unit u in units where (u != unit && u.Position == unit.Position) select u).ToList())
                    {
                        unit.Combine(u);
                        units.Remove(u);
                    }
                }
                else
                {
                    NextState();
                }
            }
        }

        private void UpdateRoundsEnd(GameTime gameTime)
        {
            Boolean doingAnything = false;
            List<Unit> unitsToRemove = new List<Unit>();
            foreach (Unit unit in units)
            {
                Point velocity = unit.Velocity;
                if (ground[unit.Position.X + velocity.X, unit.Position.Y + velocity.Y].IsWall)
                {
                    unit.TargetPosition = unit.Position;
                }
                if (unit.WantsUpdate)
                {
                    doingAnything = true;
                    unit.Update();
                    foreach (Unit u in (from Unit u in units where (u != unit && u.Position == unit.Position) select u).ToList())
                    {
                        unit.Combine(u);
                        unitsToRemove.Add(u);
                    }
                    foreach (Unit u in (from Unit u in enemies where (u.Position == unit.Position) select u).ToList())
                    {
                        unitsToRemove.Add(u);
                        unitsToRemove.Add(unit);
                    }
                }
            }
            foreach (Unit unit in unitsToRemove)
            {
                if (units.Contains(unit))
                    units.Remove(unit);
                else if (enemies.Contains(unit))
                    enemies.Remove(unit);
            }
            if (!doingAnything)
            {
                foreach (Unit unit in units)
                    unit.Reset();
                NextState();
            }

        }

        private void UpdateMoving(GameTime gameTime)
        {
            List<Unit> unitsToRemove = new List<Unit>();
            foreach (Unit unit in units)
            {
                Point velocity = unit.Velocity;
                if (ground[unit.Position.X + velocity.X, unit.Position.Y + velocity.Y].IsWall)
                {
                    unit.TargetPosition = unit.Position;
                }
                if (unit.WantsUpdate)
                {
                    unit.Update();
                    foreach (Unit u in (from Unit u in units where (u != unit && u.Position == unit.Position) select u).ToList())
                    {
                        unit.Combine(u);
                        unitsToRemove.Add(u);
                    }
                    foreach (Unit u in (from Unit u in enemies where (u.Position == unit.Position) select u).ToList())
                    {
                        unitsToRemove.Add(u);
                        unitsToRemove.Add(unit);
                    }
                }
            }
            foreach (Unit unit in unitsToRemove)
            {
                if (units.Contains(unit))
                    units.Remove(unit);
                else if (enemies.Contains(unit))
                    enemies.Remove(unit);
            }

            if (!ressources.CanSpawn)
            {
                Boolean canMove = false;
                foreach (Unit unit in units)
                {
                    if (unit.Speed > 0)
                        canMove = true;
                }
                if (!canMove)
                {
                    NextState();
                }
            }
        }

        private void UpdateMessage(GameTime gameTime)
        {
            if (messageTimer > 0) messageTimer--;
        }

        public Vector2 GetScreenPosition(Point p)
        {
            return new Vector2(p.X * tileSize, p.Y * tileSize) - cameraPosition;
        }

        public Point GetTilePosition(Vector2 v)
        {
            v += cameraPosition;
            return new Point((Int32)(v.X / tileSize), (Int32)(v.Y / tileSize));
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            switch (gameState)
            {
                case GameState.Menu:
                    DrawMenuButtons(gameTime);
                    DrawHighscores(gameTime);
                    DrawMenuAbout(gameTime);
                    break;
                case GameState.Enemy:
                case GameState.Player:
                case GameState.RoundsEnd:
                    DrawGround(gameTime);
                    DrawEnemies(gameTime);
                    DrawUnits(gameTime);
                    DrawProjectiles(gameTime);
                    DrawOverlay(gameTime);
                    DrawMessage(gameTime);
                    DrawUnitStats(gameTime);
                    break;
                case GameState.GameOver:
                    DrawGameOver(gameTime);
                    break;
                case GameState.WinningScreen:
                    DrawWinningScreen(gameTime);
                    break;
                case GameState.About:
                    DrawAbout(gameTime);
                    break;
            }
            
            spriteBatch.End();
            

            base.Draw(gameTime);
        }

        private void DrawAbout(GameTime gameTime)
        {
            Int32 i = 0, d = 32;
            spriteBatch.DrawString(aboutFont, "Defense of the corrupted Pixels", new Vector2(32, 64 + i++ * d), Color.Red);
            i++;
            spriteBatch.DrawString(aboutFont, "Made by ToolDev.de for the 26th Ludum Dare Game Jam", new Vector2(32, 64 + i++ * d), Color.White);
            spriteBatch.DrawString(aboutFont, "YSelf Tool - Code and Music", new Vector2(32, 64 + i++ * d), Color.White);
            spriteBatch.DrawString(aboutFont, "Innay Tool - Graphics and Code", new Vector2(32, 64 + i++ * d), Color.White);
            spriteBatch.DrawString(aboutFont, "Special thanks to Elayn (Conrad Tool) for testing and ideas", new Vector2(32, 64 + i++ * d), Color.White);
            i++;
            spriteBatch.DrawString(aboutFont, "Tools used:", new Vector2(32, 64 + i++ * d), Color.LightGray);
            spriteBatch.DrawString(aboutFont, "Visual Studio 2010/2012 - coding, compiling", new Vector2(32, 64 + i++ * d), Color.LightGray);
            spriteBatch.DrawString(aboutFont, "Paint.NET & Photoshop - graphics", new Vector2(32, 64 + i++ * d), Color.LightGray);
            spriteBatch.DrawString(aboutFont, "Git - Versioning", new Vector2(32, 64 + i++ * d), Color.LightGray);
            spriteBatch.DrawString(aboutFont, "cgMusic - generating music", new Vector2(32, 64 + i++ * d), Color.LightGray);
            spriteBatch.DrawString(aboutFont, "sfxr - generating sounds", new Vector2(32, 64 + i++ * d), Color.LightGray);
            spriteBatch.DrawString(aboutFont, "Lame Encoder - sounds & music", new Vector2(32, 64 + i++ * d), Color.LightGray);
            spriteBatch.DrawString(aboutFont, "PuTTY - accessing server", new Vector2(32, 64 + i++ * d), Color.LightGray);
            spriteBatch.DrawString(aboutFont, ".NET & XNA 4.0 - frameworks", new Vector2(32, 64 + i++ * d), Color.LightGray);
            i++;
            spriteBatch.DrawString(aboutFont, "Return with pressing [ENTER]", new Vector2(32, 64 + i++ * d), Color.White);
            
        }

        private void DrawUnitStats(GameTime gameTime)
        {
            foreach (Unit unit in units)
                DrawUnitStats(unit);
            foreach(Unit unit in enemies)
                DrawUnitStats(unit);
        }

        private void DrawUnitStats(Unit unit)
        {
            Vector2 p = GetScreenPosition(unit.Position);
            Rectangle rect = new Rectangle((Int32)p.X, (Int32)p.Y, textures.Pixel0.Width, textures.Pixel0.Height);
            if (rect.Contains(new Point(currentMouseState.X, currentMouseState.Y)))
            {
                DrawSingleUnitStat(unit.Life, 0, new Color(255, 255, 255));
                DrawSingleUnitStat(unit.Speed, 1, new Color(255, 0, 0));
                DrawSingleUnitStat(unit.Power, 2, new Color(0, 255, 0));
                DrawSingleUnitStat(unit.Resistance, 3, new Color(0, 0, 255));
            }
        }

        private void DrawSingleUnitStat(Int32 value, Int32 index, Color color)
        {
            spriteBatch.Draw(textures.Pixel0, new Rectangle(screenSize.X - 8 * value, 16 + index * 6, 8 * value, 4), color);
        }

        private void DrawWinningScreen(GameTime gameTime)
        {
            if(noPotato)
                spriteBatch.Draw(textures.NoPotato, new Vector2(screenSize.X / 2 - textures.NoPotato.Width / 2, screenSize.Y / 2 - textures.NoPotato.Height / 2), Color.White);
            else
                spriteBatch.Draw(textures.Potato, new Vector2(screenSize.X / 2 - textures.Potato.Width / 2, screenSize.Y / 2 - textures.Potato.Height / 2), Color.White);
            String t = "Thank you, saviour. Your screen is now sane";
            spriteBatch.DrawString(font, t, new Vector2(screenSize.X / 2 - font.MeasureString(t).X / 2, screenSize.Y / 2 + textures.NoPotato.Height / 2 + 10), Color.Gold);
            spriteBatch.DrawString(scoreFont, score.ToString(), new Vector2(screenSize.X / 2 - font.MeasureString(score.ToString()).X / 2, screenSize.Y / 2 + textures.NoPotato.Height / 2 + 100), Color.WhiteSmoke);
            if (winningScreenElapsedTime > TimeSpan.FromSeconds(2))
            {
                if(noPotato)
                    spriteBatch.DrawString(font, "#nopotato", new Vector2(screenSize.X / 2 - font.MeasureString("#nopotato").X / 2, screenSize.Y / 2 + textures.NoPotato.Height / 2 + 150), Color.White);
                else
                    spriteBatch.DrawString(font, "#votepotato", new Vector2(screenSize.X / 2 - font.MeasureString("#votepotato").X / 2, screenSize.Y / 2 + textures.NoPotato.Height / 2 + 150), Color.White);
            }
        }

        private void DrawMenuButtons(GameTime gameTime)
        {
            foreach (Button button in menuButtons)
            {
                spriteBatch.Draw(button.Texture, button.Rectangle, Color.White);
                spriteBatch.Draw(textures.Pixel0, new Vector2(button.Rectangle.Right - textures.Pixel0.Width, button.Rectangle.Center.Y - textures.Pixel0.Height / 2), button.Rectangle.Contains(new Point(currentMouseState.X, currentMouseState.Y)) ? button.Color : Color.White);
                spriteBatch.DrawString(scoreFont, button.Text, new Vector2(button.Rectangle.X + 10, button.Rectangle.Center.Y - scoreFont.MeasureString(button.Text).Y / 2), Color.Black);
            }
        }

        private void DrawMenuAbout(GameTime gameTime)
        {
            spriteBatch.DrawString(tutorialFont, tutorialText, new Vector2(250, 200), Color.LightGray);
            String t = "Defense of the corrupted Pixels";
            spriteBatch.DrawString(titleFont, t, new Vector2(screenSize.X / 2 - titleFont.MeasureString(t).X / 2, 60), Color.YellowGreen);
            spriteBatch.DrawString(aboutFont, menuAboutText, new Vector2(screenSize.X / 2, screenSize.Y * 0.9f) - aboutFont.MeasureString(menuAboutText) / 2, Color.LightGray);
        }

        private void DrawHighscores(GameTime gameTime)
        {
            spriteBatch.DrawString(scoreFont, "Highscores", new Vector2(0.77f * screenSize.X, 160), Color.White);
            Int32 i = 1;
            foreach (Tuple<String, Int32, DateTime> score in highscoreManager.Scores)
            {
                spriteBatch.DrawString(scoreFont, i + ".", new Vector2(0.77f * screenSize.X + 20 - scoreFont.MeasureString(i + ".").X, 160 + 24 * i), Color.White);
                spriteBatch.DrawString(scoreFont, score.Item2.ToString(), new Vector2(0.77f * screenSize.X + 80 - scoreFont.MeasureString(score.Item2.ToString()).X, 160 + 24 * i), Color.White);
                spriteBatch.DrawString(scoreFont, score.Item1, new Vector2(0.77f * screenSize.X + 100, 160 + 24 * i++), Color.White);
            }
        }

        private void DrawGameOver(GameTime gameTime)
        {
            spriteBatch.DrawString(font, "Game Over.", new Vector2(screenSize.X / 2 - font.MeasureString("Game Over.").X / 2, 0.25f * screenSize.Y), Color.White);
            spriteBatch.DrawString(font, score.ToString(), new Vector2(screenSize.X / 2 - font.MeasureString(score.ToString()).X / 2, 0.25f * screenSize.Y + 64), Color.White);
            
        }

        private void DrawUnits(GameTime gameTime)
        {
            foreach (Unit unit in units)
                spriteBatch.Draw(unit.Texture, GetScreenPosition(unit.Position), unit.Color);
            if (gameState == GameState.Player)
            {
                if (selectedUnit != null)
                {
                    spriteBatch.Draw(textures.SelectionBorder, GetScreenPosition(selectedUnit.Position) - new Vector2(1, 1), Color.White);
                }
                if (ressources.CanSpawn)
                {
                    Point p = new Point(currentMouseState.X / tileSize, groundSize.Y - 1);
                    spriteBatch.Draw(textures.SpawnBorder, GetScreenPosition(p), Color.White);
                }
            }
        }

        private void DrawGround(GameTime gameTime)
        {
            for (Int32 x = 0; x < groundSize.X; x++)
                for (Int32 y = 0; y < groundSize.Y; y++)
                    spriteBatch.Draw(ground[x, y].Texture, GetScreenPosition(new Point(x, y)), ground[x, y].Color);
        }

        private void DrawMessage(GameTime gameTime)
        {
            if (messageTimer > 0)
                spriteBatch.DrawString(font, message, new Vector2(screenSize.X / 2 - font.MeasureString(message).X / 2, screenSize.Y - 64 - (message.Contains("\n") ? 64 : 32)), Color.WhiteSmoke);
        }

        private void DrawOverlay(GameTime gameTime)
        {
            DrawRessource(ressources.Red, 0, Color.Red);
            DrawRessource(ressources.Green, 1, Color.Green);
            DrawRessource(ressources.Blue, 2, Color.Blue);

            spriteBatch.DrawString(scoreFont, score.ToString(), new Vector2(0.02f * screenSize.X, 0.02f * screenSize.Y), Color.WhiteSmoke);
        }

        private void DrawRessource(Int32 value, Int32 index, Color color)
        {
            spriteBatch.Draw(textures.Ressources, new Rectangle(0, screenSize.Y - 12 * index - 12, (value * 4), 4 + (ressources.CanSpawn ? 4 : 0)), color);
        }

        private void DrawEnemies(GameTime gameTime)
        {
            foreach (Unit unit in enemies)
                spriteBatch.Draw(unit.Texture, GetScreenPosition(unit.Position), unit.Color);
        }

        private void DrawProjectiles(GameTime gameTime)
        {
            foreach (Projectile p in projectiles)
                spriteBatch.Draw(textures.Projectile, p.Position - cameraPosition, p.Color);
        }
    }
}
