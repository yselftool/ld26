﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Xml.Linq;
using System.Security.Cryptography;

namespace LudumDare26
{
    class HighscoreManager
    {
        private List<Tuple<String, Int32, DateTime>> scores;
        private Int32 pullState;
        private Boolean pushedOne = false;

        public Boolean PushedOne
        {
            get { return pushedOne; }
        }

        public HighscoreManager()
        {
            scores = new List<Tuple<string, int, DateTime>>();
            
        }

        public void BeginPull()
        {
            ThreadStart start = new ThreadStart(Download);
            Thread thread = new Thread(start);
            thread.Start();
        }

        public void BeginPush(Tuple<String, Int32> score)
        {
            pushedOne = true;
            ParameterizedThreadStart start = new ParameterizedThreadStart(Upload);
            Thread thread = new Thread(start);
            thread.Start(score);
        }

        private void Upload(Object arg)
        {
            try
            {
                if (arg is Tuple<String, Int32>)
                {
                    StringBuilder b = new StringBuilder();
                    b.Append((char)0x68);
                    b.Append((char)0x74);
                    b.Append((char)0x74);
                    b.Append((char)0x70);
                    b.Append((char)0x3A);
                    b.Append((char)0x2F);
                    b.Append((char)0x2F);
                    b.Append((char)0x74);
                    b.Append((char)0x6F);
                    b.Append((char)0x6F);
                    b.Append((char)0x6C);
                    b.Append((char)0x64);
                    b.Append((char)0x65);
                    b.Append((char)0x76);
                    b.Append((char)0x2E);
                    b.Append((char)0x64);
                    b.Append((char)0x65);
                    b.Append((char)0x2F);
                    b.Append((char)0x70);
                    b.Append((char)0x6C);
                    b.Append((char)0x61);
                    b.Append((char)0x79);
                    b.Append((char)0x67);
                    b.Append((char)0x72);
                    b.Append((char)0x6F);
                    b.Append((char)0x75);
                    b.Append((char)0x6E);
                    b.Append((char)0x64);
                    b.Append((char)0x2F);
                    b.Append((char)0x6C);
                    b.Append((char)0x64);
                    b.Append((char)0x32);
                    b.Append((char)0x36);
                    b.Append((char)0x2F);
                    b.Append((char)0x70);
                    b.Append((char)0x75);
                    b.Append((char)0x73);
                    b.Append((char)0x68);
                    b.Append((char)0x2E);
                    b.Append((char)0x70);
                    b.Append((char)0x68);
                    b.Append((char)0x70);



                    String td = b.ToString();
                    Tuple<String, Int32> score = (Tuple<String, Int32>)arg;
                    MD5 hasher = MD5.Create();
                    String s = score.Item1 + "." + score.Item2.ToString() + "s4lt";
                    Byte[] data = hasher.ComputeHash(Encoding.Default.GetBytes(s));
                    StringBuilder builder = new StringBuilder();
                    for (Int32 i = 0; i < data.Length; i++)
                    {
                        builder.Append(data[i].ToString("x2"));
                    }
                    Uri address = new Uri(td + "?n=" + score.Item1 + "&s=" + score.Item2 + "&h=" + builder.ToString());
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                }
            }
            catch (Exception e) { }
        }

        private void Download()
        {
            try
            {
                StringBuilder b = new StringBuilder();
                b.Append((char)0x68);
                b.Append((char)0x74);
                b.Append((char)0x74);
                b.Append((char)0x70);
                b.Append((char)0x3A);
                b.Append((char)0x2F);
                b.Append((char)0x2F);
                b.Append((char)0x74);
                b.Append((char)0x6F);
                b.Append((char)0x6F);
                b.Append((char)0x6C);
                b.Append((char)0x64);
                b.Append((char)0x65);
                b.Append((char)0x76);
                b.Append((char)0x2E);
                b.Append((char)0x64);
                b.Append((char)0x65);
                b.Append((char)0x2F);
                b.Append((char)0x70);
                b.Append((char)0x6C);
                b.Append((char)0x61);
                b.Append((char)0x79);
                b.Append((char)0x67);
                b.Append((char)0x72);
                b.Append((char)0x6F);
                b.Append((char)0x75);
                b.Append((char)0x6E);
                b.Append((char)0x64);
                b.Append((char)0x2F);
                b.Append((char)0x6C);
                b.Append((char)0x64);
                b.Append((char)0x32);
                b.Append((char)0x36);
                b.Append((char)0x2F);
                b.Append((char)0x70);
                b.Append((char)0x75);
                b.Append((char)0x6C);
                b.Append((char)0x6C);
                b.Append((char)0x2E);
                b.Append((char)0x70);
                b.Append((char)0x68);
                b.Append((char)0x70);

                String td = b.ToString();
                Uri address = new Uri(td);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                XDocument doc = XDocument.Load(response.GetResponseStream());
                List<Tuple<String, Int32, DateTime>> list = new List<Tuple<String, Int32, DateTime>>();
                foreach (XElement elem in doc.Element("scores").Elements("s"))
                {
                    list.Add(new Tuple<String, Int32, DateTime>(
                        elem.Element("n").Value,
                        Int32.Parse(elem.Element("s").Value),
                        DateTime.Parse(elem.Element("d").Value)));
                }
                lock (scores)
                {
                    scores = list;
                }
                pullState++;
            }
            catch (Exception e) { }
        }

        public List<Tuple<String, Int32, DateTime>> Scores
        {
            get { return scores; }
        }

        public Int32 PullState
        {
            get { return pullState; }
        }


    }
}
