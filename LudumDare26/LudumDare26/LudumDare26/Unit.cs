﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LudumDare26
{
    class Unit
    {
        private Texture2D texture;
        private Point position;
        private Point targetPosition;

        private Color color;

        private Int32 life;
        private Int32 speed, maxSpeed;
        private Int32 power, maxPower;
        private Int32 resistance, maxResistance;

        public Unit(Texture2D texture, Point position, Color color, Int32 life, Int32 speed, Int32 power, Int32 resistance)
        {
            this.texture = texture;
            this.position = position;
            this.targetPosition = position;
            this.color = color;
            this.life = life;
            this.speed = speed; this.maxSpeed = speed;
            this.power = power; this.maxPower = power;
            this.resistance = resistance; this.maxResistance = resistance;
        }

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public Point Position
        {
            get { return position; }
            set { position = value; }
        }

        public Point TargetPosition
        {
            get { return targetPosition; }
            set { targetPosition = value; }
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        public Int32 Life
        {
            get { return life; }
            set { life = value; }
        }

        public Int32 Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public Int32 Power
        {
            get { return power; }
            set { power = value; }
        }

        public Int32 Resistance
        {
            get { return resistance; }
            set { resistance = value; }
        }

        public Int32 Range
        {
            get
            {
                return (Int32)(3 * Math.Log(maxSpeed + maxPower + maxResistance));
            }
        }

        public Int32 Distance(Unit unit)
        {
            Point delta = new Point(this.Position.X - unit.Position.X, this.Position.Y - unit.Position.Y);
            return (Int32)Math.Sqrt(delta.X * delta.X + delta.Y * delta.Y);
        }

        public void ReduceHealth(Int32 amount)
        {
            if (resistance > 0)
                resistance -= amount;
            if(resistance <= 0)
                life--;
        }

        public void ReduceAfterRound()
        {
            life /= 2;
            maxPower = (Int32)(maxPower * 0.75f);
            maxSpeed = (Int32)(maxSpeed * 0.75f);
            maxResistance = (Int32)(maxResistance * 0.75f);
            color = new Color(color.R * 0.75f, color.G * 0.75f, color.B * 0.75f);
        }

        public static Unit CreateRed(Texture2D texture, Point position)
        {
            return new Unit(texture,
                position,
                new Color(255, 0, 0),
                2,
                8,
                2,
                2);
        }

        public static Unit CreateGreen(Texture2D texture, Point position)
        {
            return new Unit(texture,
                position,
                new Color(0, 255, 0),
                2,
                5,
                5,
                2);
        }

        public static Unit CreateBlue(Texture2D texture, Point position)
        {
            return new Unit(texture,
                position,
                new Color(0, 0, 255),
                2,
                5,
                2,
                5);
        }

        public static Unit CreateRandom(Random random, Texture2D texture, Point position)
        {
            if (random.Next(2) == 0)
            {
                switch (random.Next(3))
                {
                    case 0:
                        return CreateRed(texture, position);
                    case 1:
                        return CreateGreen(texture, position);
                    case 2:
                        return CreateBlue(texture, position);
                }
                return CreateGreen(texture, position);
            }
            else
            {
                Int32 r = random.Next(5) + 1;
                Int32 g = random.Next(5) + 1;
                Int32 b = random.Next(5) + 1;

                return new Unit(texture, position, new Color(r * 30, g * 30, b * 30), (r + g + b) / 2, r, g, b);
            }
        }

        private static Unit CreateCheap(Ressources res, Texture2D texture, Point position)
        {
            Boolean enough = false;
            if (res.Red >= 2)
            {
                enough = true;
                res.Red -= 2;
            }
            if (res.Green >= 2)
            {
                enough = true;
                res.Green -= 2;
            }
            if (res.Blue >= 2)
            {
                enough = true;
                res.Blue -= 2;
            }

            if (enough)
            {
                return new Unit(texture, position, Color.Gray, 1, 3, 1, 1);
            }
            else
            {
                return null;
            }
        }

        public static Unit Create(Ressources res, Texture2D texture, Point position)
        {
            if (res.Red >= res.Green && res.Red >= res.Blue)
            {
                if (res.Red >= 5)
                {
                    res.Red -= 5;
                    return CreateRed(texture, position);

                }
            }
            else if (res.Green >= res.Red && res.Green >= res.Blue)
            {
                if (res.Green >= 5)
                {
                    res.Green -= 5;
                    return CreateGreen(texture, position);

                }
            }
            else if (res.Blue >= res.Green && res.Blue >= res.Red)
            {
                if (res.Blue >= 5)
                {
                    res.Blue -= 5;
                    return CreateBlue(texture, position);

                }
            }
            return CreateCheap(res, texture, position);
        }

        public void Combine(Unit unit)
        {
            this.Color = new Color((this.Color.R / 2 + unit.Color.R / 2), (this.Color.G / 2 + unit.Color.G / 2), (this.Color.B / 2 + unit.Color.B / 2));
            this.Life += unit.Life;
            this.Speed += unit.Speed; this.maxSpeed += unit.maxSpeed; this.maxSpeed = (Int32)(maxSpeed * 0.75f);
            this.Power += unit.Power; this.maxPower += unit.maxPower; this.maxPower = (Int32)(maxPower * 0.75f);
            this.Resistance += unit.Resistance; this.maxResistance += unit.maxResistance; this.maxResistance = (Int32)(maxResistance * 0.75f);
        }

        public void Reset()
        {
            this.Speed = maxSpeed;
            this.Power = maxPower;
            this.maxResistance = (Resistance + maxResistance) / 2;
            this.Resistance = maxResistance;
        }

        public Boolean WantsUpdate
        {
            get
            {
                return WantsMove();
            }
        }

        public void Update()
        {
            if (WantsMove())
                Move();

        }

        private Boolean WantsMove()
        {
            Point delta = new Point(targetPosition.X - position.X, targetPosition.Y - position.Y);
            return speed > 0 && (delta.X != 0 || delta.Y != 0);
        }

        public Point Velocity
        {
            get
            {
                Point delta = new Point(targetPosition.X - position.X, targetPosition.Y - position.Y);
                if ((delta.X != 0 || delta.Y != 0) && speed > 0)
                {
                    if (Math.Abs(delta.X) > Math.Abs(delta.Y))
                    {
                        if (delta.X < 0)
                            return new Point(-1, 0);
                        else
                            return new Point(1, 0);
                    }
                    else
                    {
                        if (delta.Y < 0)
                            return new Point(0, -1);
                        else
                            return new Point(0, 1);
                    }
                }
                return new Point(0, 0);
            }
        }

        private void Move()
        {
            Point delta = new Point(targetPosition.X - position.X, targetPosition.Y - position.Y);
            if((delta.X != 0 || delta.Y != 0) && speed > 0)
            {
                if (Math.Abs(delta.X) > Math.Abs(delta.Y))
                {
                    if (delta.X < 0)
                        position.X -= 1;
                    else
                        position.X += 1;
                }
                else
                {
                    if (delta.Y < 0)
                        position.Y -= 1;
                    else
                        position.Y += 1;
                }
                this.speed--;
            }
        }

        public Unit Clone()
        {
            return new Unit(texture, position, color, life, speed, power, resistance);
        }
    }
}
