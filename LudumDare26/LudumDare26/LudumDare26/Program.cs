using System;

namespace LudumDare26
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (LD26Game game = new LD26Game())
            {
                game.Run();
            }
        }
    }
#endif
}

