﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LudumDare26
{
    class Projectile
    {
        private Color color;
        private Vector2 startPos;
        private Vector2 endPos;
        private Int32 length;
        private Int32 timer;

        public Projectile(Color color, Int32 length, Vector2 startPos, Vector2 endPos)
        {
            this.color = color;
            this.length = length;
            this.startPos = startPos;
            this.endPos = endPos;
            this.timer = 0;
        }

        public Int32 Timer
        {
            get { return timer; }
            set { timer = value; }
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }


        public Vector2 Position 
        {
            get
            {
                return Vector2.Lerp(startPos, endPos, (float)timer / (float)length);
            }
        }

        public Boolean Living
        {
            get
            {
                return timer <= length;
            }
        }

    }
}
