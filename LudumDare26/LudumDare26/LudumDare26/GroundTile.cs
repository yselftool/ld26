﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LudumDare26
{
    class GroundTile
    {
        private Texture2D texture;
        private Boolean wall;
        private Color color;

        public GroundTile(Texture2D texture, Boolean wall, Color color)
        {
            this.texture = texture;
            this.wall = wall;
            this.color = color;
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public Boolean IsWall
        {
            get { return wall; }
            set { wall = value; }
        }


    }
}
