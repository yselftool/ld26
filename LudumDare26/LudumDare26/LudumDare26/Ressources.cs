﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LudumDare26
{
    class Ressources
    {
        private Int32 red;
        private Int32 green;
        private Int32 blue;

        public Ressources(Int32 red, Int32 green, Int32 blue)
        {
            this.red = red;
            this.green = green;
            this.blue = blue;
        }

        public Int32 Blue
        {
            get { return blue; }
            set { blue = value; }
        }

        public Int32 Green
        {
            get { return green; }
            set { green = value; }
        }

        public Int32 Red
        {
            get { return red; }
            set { red = value; }
        }

        public Boolean IsBiggerThan(Ressources res)
        {
            return this.Red >= res.Red &&
                this.Green >= res.Green &&
                this.Blue >= res.Blue;
        }

        public void Reduce(Ressources res)
        {
            this.Red -= res.Red;
            this.Green -= res.Green;
            this.Blue -= res.Blue;
        }

        public Boolean CanSpawn
        {
            get
            {
                return red >= 2 || green >= 2 || blue >= 2;
            }
        }

    }
}
