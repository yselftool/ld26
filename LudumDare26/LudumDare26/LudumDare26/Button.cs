﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LudumDare26
{
    class Button
    {
        private Texture2D texture;
        private Rectangle rect;
        private String text;
        private Color color;

        
        public Button(Texture2D texture, Point position, String text, Color color)
        {
            this.texture = texture;
            this.rect = new Rectangle(position.X, position.Y, texture.Width, texture.Height);
            this.text = text;
            this.color = color;
        }

        public Rectangle Rectangle
        {
            get { return rect; }
        }

        public Texture2D Texture
        {
            get { return texture; }
        }

        public String Text
        {
            get { return text; }
        }

        public Color Color
        {
            get { return color; }
        }


    }
}
