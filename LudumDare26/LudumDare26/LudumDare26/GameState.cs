﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LudumDare26
{
    enum GameState
    {
        Menu,
        Player,
        RoundsEnd,
        Enemy,
        GameOver,
        WinningScreen,
        About
    }
}
